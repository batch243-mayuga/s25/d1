// console.log("Hello JSON!");

// [Section] JSON Objects
/*
	-JSON stands for Javascript Object Notation
	-JSON is also used in other programming languages hence the name JSON
	-Core Javascript has a built in JSON object that contains methods for parsing JSON objects and converting strings into Javascript objects.
	-JSON is used for serializing different data types.
	Syntax:
		{
			"propertyA": "valueA",
			"propertyB": "valueB"
		}
*/

// JSON Object example:
/*{
	"city" : "Quezon City",
	"province" : "Metro Manila",
	"country" : "Philippines"
}*/

// JSON Arrays example:
/*[	
	{
		"city": "Quezon City", 
		"province": "Metro Manila", 
		"country": "Philippines"
	} 
	{	
		"city": "Manila City", 
		"province": "Metro Manila", 
		"country": "Philippines"
	}
]*/

// [Section] JSON Methods
	// The JSON object contains method for parsing and converting data into stringified JSON

	let batchesArr = [
						{
							batchName: "Batch X"
							},
						{
							batchName: "Batch Y"
							}
						];
					console.log("This is the original array:");
					console.log(batchesArr)
	// The stringify method is used to convert Javascript objects into a string
	console.log("Result from stringify method:")
	let stringBatchesArr = JSON.stringify(batchesArr);
	console.log(stringBatchesArr);
	console.log(typeof stringBatchesArr);
// direct
	let data = JSON.stringify(
	{
		name: 'John',
		address: {
			city: "Manila",
			country: "Philippines"
		}
	})
	console.log(data);

// [Section] Using stringify Method with variables
// When information is stored in a variable and is not hard coded into an object that is being stringified, we can supply the value with a variable

// User details sample
/*let firstName = prompt("What is your first name?");
let lastName = prompt("What is your last name?");
let age = prompt("How young are you?");
let address = {
	city: prompt("Which city do you live in?"),
	country: prompt("Which country does your city adress belong?")
}

let otherData = JSON.stringify({
	firstName, 
	lastName,
	age,
	address
})*/

/*console.log(otherData);*/

// [Section] Converting stringified JSON into Javascript Objects

	/*
		-Objects are common data types used in application because of the complex data structures that can be created out of them
		-Information is commonly sent to applications in stringified JSON and then converted back into objects
		-This happens both for sending information to a backend application and sending information back to a frontend application.
	*/

	let batchesJSON = `[{"batchName":"Batch X"},{"batchName":"Batch Y"}]`;
	let parseBatchesJSON = JSON.parse(batchesJSON);
	console.log(parseBatchesJSON);

	console.log(parseBatchesJSON[0]);

	let stringifiedObject = `{
			"name" : "John",
			"age" : "31",
			"address" : {
				"city" : "Manila",
				"country" : "Philippines"
			}
		}`

		console.log(JSON.parse(stringifiedObject));